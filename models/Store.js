const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const storeSchema = new Schema( {
    name: {type: String, required: true},
    location: {type: String, required: true},
    car: [{type: mongoose.Types.ObjectId, ref: 'Car'}],
}, {
    timestamps: true,
});

const Store = mongoose.model('Store', storeSchema);

module.exports = Store;