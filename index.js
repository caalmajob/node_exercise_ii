const express = require('express');
const indexRoutes = require('./routes/index.routes');
const carRoutes = require('./routes/cars.routes');
const storeRoutes = require('./routes/store.routes');

const db = require('./db.js');
db.connect();
const PORT = 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use('/', indexRoutes);
app.use('/cars', carRoutes);
app.use('/store', storeRoutes);


app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    return res.status(error.status || 500).json(error.message || 'Unexpected error');
});

app.listen(PORT, () => {
    console.log(`Server listening in http://localhost:${PORT}`)
});

/**
 * 1. Crea un servidor de node con todos sus detalles.
 * 2. Crea un modelo de coches con al menos 4 campos.
 * 3. Tienes que definir el router en una carpeta aparte.
 * 4. Crea un CRUD con el modelo de vehículos.
 */