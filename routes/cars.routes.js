const express = require('express');
const Car = require('../models/Car');

const router = express.Router();


router.get('/', async (req, res) => {
    try {
    const cars = await Car.find();
    return res.json(cars);
    } catch(error) {
        console.log(error);
        next(new Error(error));
    }
})

router.get('/:id', async (req, res) => {
    try {
    const { id } = req.params;
    const car = await Car.findById(id);
    if(car) {
        return res.json(car);
    } else {
        return res.status(404).json("No car found for this id")
    }
    } catch(error) {
        console.log(error);
        next(new Error(error));
    }
});

router.delete('/:id', async (req, res) => {
    try {
    const { id } = req.params;
    const deleted = await Car.findByIdAndDelete(id);
    if(deleted) {
        return res.status(200).json('Car deleted');
    }
        return res.status(200).json("Car not found");
    } catch(error) {
        next(error);
    }
});

router.get('/type/:type', async (req, res) => {
    try {
    const { type } = req.params;
    const carsByType = await Car.find( {type});
    return res.json(carsByType);
    } catch(error) {
        next(new Error(error));
    }
});

router.post('/create', async (req, res, next) => {
    try {
        const { brand, model, type, color } = req.body;

        const newCar = await new Car({brand, model, type, color});

        const createdCar = await newCar.save();

        return res.json(createdCar);
    } catch (error) {
        next(error);
    }
});

module.exports = router;