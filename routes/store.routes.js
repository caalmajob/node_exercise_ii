const express = require('express');
const Store = require('../models/Store');

const router = express.Router();


router.get('/', (request, response) => {
    return response.send('Store Page');
});

router.post('/create', async (req, res, next) => {
    try {
        const { name, location } = req.body;

        const newStore = await new Store({name, location});

        const createdStore = await newStore.save();

        return res.json(createdStore);
    } catch (error) {
        next(error);
    }
});

router.put('/add-car', async (req, res, next) => {
    try {
        const { storeId, carId } = req.body;

        const updatedStore = await Store.findByIdAndUpdate(
            storeId,
            { $push: { car: carId } },
            { new: true }
        );

        return res.json(updatedStore);

    } catch(error) {
        next(error);
    }
});


router.get('/:id', async (req, res, next) => {
    try {
        // Buscar un concesionario y devolver todos los coches que tiene.

        const { id } = req.params;

        const store = await Store.findById(id).populate('car')

        return res.json(store);

    } catch(error) {
        next(error);
    }
})


module.exports = router;