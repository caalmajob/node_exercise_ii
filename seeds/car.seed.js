const mongoose = require('mongoose');
const db = require('../db');
const Car = require('../models/Car');

const cars = [
    {
        brand: "Renault",
        model: "Clio",
        type: "Sedan",
        color: "Red",
},
{
    brand: "Mercedes",
    model: "C5",
    type: "Sedan",
    color: "Grey",
},
{
    brand: "Kia",
    model: "Picanto",
    type: "Famlily",
    color: "Blue",
},
{
    brand: "Toyota",
    model: "Clio",
    type: "Suv",
    color: "Black",
},
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
    // Buscar los coches existentes en nuestra base de datos.
    //Si tenemos coches, los borramos. Si no tenemos coches, continuamos ejecutando.
    console.log('Deleting all cars...');
    const allCars = await Car.find();
    

    if(allCars.length) {
        await Car.collection.drop();
    }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then( async() => {
        //Añadimos a nuestra base de datos los coches del seed.
        await Car.insertMany(cars);
        console.log('Successfully added cars to DB...');
    })
    .finally(() => mongoose.disconnect());